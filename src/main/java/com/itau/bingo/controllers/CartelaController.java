package com.itau.bingo.controllers;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.itau.bingo.models.Cartela;

@RestController
public class CartelaController {

	@RequestMapping("/cartela")
	public ResponseEntity<Cartela> getCartela(){
		Cartela cartela = new Cartela();
		return ResponseEntity.ok(cartela);
	}
	
}
