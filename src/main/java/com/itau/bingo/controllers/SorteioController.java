package com.itau.bingo.controllers;

import java.util.ArrayList;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.itau.bingo.models.Mensagem;
import com.itau.bingo.models.Sorteio;

@RestController
public class SorteioController {

	public static ArrayList<Integer> numeroSorteados = new ArrayList<Integer>();
	
	@RequestMapping("/sorteio")
	public ResponseEntity<?> getSorteio() {		
		if (numeroSorteados.size() >= 60) {
			Mensagem erro = new Mensagem();
			erro.mensagem = "O sorteio acabou";
			return ResponseEntity.status(500).body(erro);
		} else {
			Sorteio sorteio = new Sorteio();
			sorteio.setNumero(sortearUnico());
			return ResponseEntity.ok(sorteio);			
		}		
	}
	
	@RequestMapping("/reiniciar")
	public ResponseEntity<Mensagem> reiniciar() {
		numeroSorteados = new ArrayList<>();
		Mensagem mensagem = new Mensagem();
		mensagem.mensagem = "Sorteio reiniciado";
		return ResponseEntity.ok(mensagem);
	}
	
	private static int sortearUnico() {
		double sorteio = Math.random();
		int sorteado = (int) Math.ceil(sorteio * 60);
		boolean numeroUnico = false;
		
		 while(numeroUnico == false){
			if (numeroSorteados.contains(sorteado) == false) {
				numeroUnico = true;
				numeroSorteados.add(sorteado);
			}
			else if (numeroSorteados.size() >= 60){
				numeroUnico = true;
				System.out.println("Todos os números já foram sorteados!");
			}
			else {
				sorteio = Math.random();
				sorteado = (int) Math.ceil(sorteio * 60);
			}
		}
		return sorteado;
	}
	
}
