package com.itau.bingo.models;

import java.util.ArrayList;

public class Cartela {

	private String titulo;
		
	private int[][] conteudo;
	
	private ArrayList<Integer> numeroSorteados = new ArrayList<Integer>();
	
	public Cartela() {
		int[][] Cartela = new int[4][4];
		for (int i = 0; i < 4; i++) {
			for (int j = 0; j < 4; j++) {
//				Numeros aleatorios (de 1 a 60)
				double sorteio = Math.random();
				sorteio = Math.ceil(sorteio * 60);
				Cartela[i][j] = sortear();
			}
		}
		conteudo = Cartela;
		titulo = "Bingo";
	}
	
	private int sortear () {
		double sorteio = Math.random();
		int sorteado = (int) Math.ceil(sorteio * 60);
		boolean numeroUnico = false;
		
		while(numeroUnico == false){
			if (numeroSorteados.contains(sorteado) == false) {
				numeroUnico = true;
				numeroSorteados.add(sorteado);
			}
			else if (numeroSorteados.size() >= 60){
				numeroUnico = true;
				System.out.println("Todos os números já foram sorteados!");
			}
			else {
				sorteio = Math.random();
				sorteado = (int) Math.ceil(sorteio * 60);
			}
		}
		return sorteado;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public int[][] getConteudo() {
		return conteudo;
	}

	public void setConteudo(int[][] conteudo) {
		this.conteudo = conteudo;
	}
	
}
